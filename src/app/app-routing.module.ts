import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RuleEditorComponent } from './rule-editor/rule-editor.component';
import { EditRuleComponent } from './rule-editor/edit-rule/edit-rule.component';
import { TestDataComponent } from './test-data/test-data.component';
import { DataManagementComponent } from './data-management/data-management.component';
import { DataObjectsComponent } from './data-objects/data-objects.component';

const routes: Routes = [
        {path: '', redirectTo: '/data-management', pathMatch: 'full'},
        {path: 'data-management', component: DataManagementComponent},
        {path: 'data-management/data-objects/:object', component: DataObjectsComponent},
        {
            path: 'data-management/data-objects/:object/:property/validation', component: RuleEditorComponent, children: [
                {path: '', component: TestDataComponent, pathMatch: 'full'},
                {path: 'edit/:ruleId', component: EditRuleComponent}
            ]
        }

        // {
        //     path: 'data-management/data-objects/:object', component: DataObjectsComponent, children: [
        //         {
        //             path: ':property/validation',
        //             component: RuleEditorComponent,
        //             children: [
        //                 {path: '', component: TestDataComponent, pathMatch: 'full'},
        //                 {path: 'edit/:ruleId', component: EditRuleComponent}
        //             ]
        //         }
        //     ]
        // },

    ]
;

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
