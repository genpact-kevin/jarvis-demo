import { Expression } from "../expression.model";

export class Identifier extends Expression {
  type = 'Identifier';

  constructor (
      public name: string,
      public dataType?: string
  ) {
    super();
  }
}
