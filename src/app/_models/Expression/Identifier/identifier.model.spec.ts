import { Identifier } from "./identifier.model";

describe('Identifier', () => {
  /** @test Identifier can be initialized
   * When a Identifier object is initialized
   * Then checking if it exists should yield true
   */
  it('can be initialized', () => {
    expect(new Identifier('foo')).toBeTruthy();
  });

  /** @test Identifier name should be a string
   * Given an existing Identifier
   * When we check the type of the Identifier name
   * Then we should see that it is a string
   */
  it('name should be a string', () => {
    const identifier = new Identifier('foo');
    expect(typeof identifier.name).toBe('string');
  });
});
