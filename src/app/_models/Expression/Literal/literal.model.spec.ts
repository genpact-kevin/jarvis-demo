import { Literal } from "./literal.model";

describe('Literal', () => {
  /** @test Literal can be initialized
   * When a Literal object is initialized
   * Then checking if it exists should yield true
   */
  it('can be initialized', () => {
    expect(new Literal('this is a string')).toBeTruthy();
  });

  /** @test Literal can be a string
   * Given an existing Literal of type string
   * When we check the kind of Literal
   * Then we should see 'string'
   */
  it('can be a string', () => {
    const literal = new Literal('this is a string');
    expect(literal.kind).toEqual('string');
  });

  /** @test Literal can be a number
   * Given an existing Literal of type number
   * When we check the kind of Literal
   * Then we should see 'number'
   */
  it('can be a number', () => {
    const literal = new Literal(7);
    expect(literal.kind).toEqual('number');
  });

  /** @test Literal can be a boolean
   * Given an existing Literal of type boolean
   * When we check the kind of Literal
   * Then we should see 'boolean'
   */
  it('can be a boolean', () => {
    const literal = new Literal(true);
    expect(literal.kind).toEqual('boolean');
  });

  /** @test Literal can be null
   * Given an existing Literal of type null
   * When we check the kind of Literal
   * Then we should see 'null'
   */
  it('can be null', () => {
    const literal = new Literal(null);
    expect(literal.kind).toEqual('null');
  });

  /** @test Literal can be a RegExp
   * Given an existing Literal of type RegExp
   * When we check the kind of Literal
   * Then we should see 'RegExp'
   */
  it('can be RegExp', () => {
    const literal = new Literal(/.*/i);
    expect(literal.kind).toEqual('RegExp');
  });
});
