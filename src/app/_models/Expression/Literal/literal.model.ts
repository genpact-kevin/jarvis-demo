import { Expression } from "../expression.model";

/**
 * Defines a Rule literal and it's type
 * Supported literal types:
 *     - string
 *     - number
 *     - boolean
 *     - RegExp
 *     - null
 */
export class Literal extends Expression {
  type = 'Literal';
  value: string | number | boolean | RegExp | null;
  kind: string;

  constructor(value?: string | number | boolean | RegExp | null) {
    super();
    this.value = value;
    this.kind = this.getKind();
  }

  /**
   * Gets the data type of value
   * @returns string "string" | "number" | "boolean" | "RegExp" | "null"
   */
  private getKind(): string {
    return (this.value === null)
      ? 'null'
      : (this.value.constructor.name === 'RegExp')
      ? 'RegExp'
      : typeof this.value;
  }
}
