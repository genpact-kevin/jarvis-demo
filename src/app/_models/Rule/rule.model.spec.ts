import { Rule } from "./rule.model";
import { Identifier } from "../Expression/Identifier/identifier.model";
import { OperatorType } from "../Operator/OperatorList/OperatorType/operator-type.model";
import { Literal } from "../Expression/Literal/literal.model";

describe('Rule', () => {
  /** @test Rule can be initialized
   * When a new Rule object is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(new Rule()).toBeTruthy();
  });

  /** @test Rule can be initialized with data
   * Given an existing Identifier and Operator
   * When passed to the Rule constructor
   * Then the Identifier and Operator should be visible on the Rule
   */
  it('can be initialized with data', () => {
    const identifier = new Identifier('foo');
    const operator = OperatorType.get('!');
    const rule = new Rule({
      left: identifier,
      operator: operator
    });
    expect(rule.getLeft()).toEqual(identifier);
    expect(rule.getOperator()).toEqual(operator);
  });

  /** @test Rule can have an Expression set to left
   * Given an existing Rule object
   * When a new Expression is set to the left property
   * Then the new left Expression should be visible
   */
  it('can have an Expression set to left', () => {
    const rule = new Rule();
    const literal = new Literal(true);
    rule.setLeft(literal);
    expect(rule.getLeft()).toEqual(literal);
  });

  /** @test Rule can have an Operator set to operator
   * Given an existing Rule object
   * When a new Operator is set to the operator property
   * Then the new Operator should be visible
   */
  it('can have an Operator set to operator', () => {
    const rule = new Rule();
    const operator = OperatorType.get('==');
    rule.setOperator(operator);
    expect(rule.getOperator()).toEqual(operator);
  });

  /** @test Rule can have an Expression set to right
   * Given an existing Rule object
   * When a new Expression is set to the right property
   * Then the new Expression should be visible
   */
  it('can have an Expression set to right', () => {
    const rule = new Rule();
    const literal = new Literal('Hello, World!');
    rule.setRight(literal);
    expect(rule.getRight()).toEqual(literal);
  });

  /** @test Rule will throw and error if a non Expression is set to left
   * Given an existing Rule object
   * And an existing Operator object
   * When the Operator is set to the left property
   * Then an error should be thrown
   */
  it('will throw and error if a non Expression is set to left', () => {
    const rule = new Rule();
    const operator = OperatorType.get('>');
    expect(() => {rule.setLeft(operator)})
      .toThrowError('The left side of a rule was expecting type Expression, but received object');
  });

  /** @test Rule will throw and error if a non Operator is set to operator
   * Given an existing Rule object
   * And an existing Expression object
   * When the Expression is set to the operator property
   * Then an error should be thrown
   */
  it('will throw and error if a non Operator is set to operator', () => {
    const rule = new Rule();
    const expression = new Literal(false);
    // @ts-ignore
    expect(() => {rule.setOperator(expression)})
      .toThrowError('The operator of a rule was expecting type Operator, but received object');
  });

  /** @test Rule will throw and error if a non Expression is set to right
   * Given an existing Rule object
   * And an existing Operator object
   * When the Operator is set to the right property
   * Then an error should be thrown
   */
  it('will throw and error if a non Expression is set to right', () => {
    const rule = new Rule();
    const operator = OperatorType.get('>');
    expect(() => {rule.setRight(operator)})
      .toThrowError('The right side of a rule was expecting type Expression, but received object');
  });
});
