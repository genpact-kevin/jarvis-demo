import { Injectable } from '@angular/core';
import { Rule } from "./rule.model";
import { Subject } from "rxjs";
import { RuleGroup } from "../RuleGroup/rule-group.model";
import { Identifier } from '../Expression/Identifier/identifier.model';
import { OperatorType } from '../Operator/OperatorList/OperatorType/operator-type.model';
import { Literal } from '../Expression/Literal/literal.model';

@Injectable({
    providedIn: 'root'
})
export class RuleService {
    onRulesChanged = new Subject<RuleGroup[]>();
    private rules: RuleGroup[];
    // private rules = [new RuleGroup()];
    // private rules = [
    //   new RuleGroup({
    //     rules: [
    //       new Rule({
    //         left: new Identifier('isActive'),
    //         operator: OperatorType.get('>='),
    //         right: new Literal(75)
    //       }),
    //       new Rule({
    //         left: new Identifier('isActive'),
    //         operator: OperatorType.get('>='),
    //         right: new Literal(50)
    //       }),
    //       new Rule({
    //         left: new Identifier('isActive'),
    //         operator: OperatorType.get('>='),
    //         right: new Literal(25)
    //       }),
    //       new Rule({
    //         left: new Identifier('isActive'),
    //         operator: OperatorType.get('>='),
    //         right: new Literal(0)
    //       })
    //     ]
    //   }),
    //   new RuleGroup({
    //     rules: [
    //       new RuleGroup({
    //         rules: [
    //           new Rule({
    //             left: new Identifier('isActive'),
    //             operator: OperatorType.get('>='),
    //             right: new Literal(75)
    //           }),
    //           new Rule({
    //             left: new Identifier('isActive'),
    //             operator: OperatorType.get('>='),
    //             right: new Literal(50)
    //           }),
    //           new Rule({
    //             left: new Identifier('isActive'),
    //             operator: OperatorType.get('>='),
    //             right: new Literal(25)
    //           }),
    //           new Rule({
    //             left: new Identifier('isActive'),
    //             operator: OperatorType.get('>='),
    //             right: new Literal(0)
    //           })
    //         ]
    //       }),
    //       new Rule({
    //         left: new Identifier('isActive'),
    //         operator: OperatorType.get('>='),
    //         right: new Literal(50)
    //       }),
    //       new Rule({
    //         left: new Identifier('isActive'),
    //         operator: OperatorType.get('>='),
    //         right: new Literal(25)
    //       }),
    //       new Rule({
    //         left: new Identifier('isActive'),
    //         operator: OperatorType.get('>='),
    //         right: new Literal(0)
    //       })
    //     ]
    //   })
    // ];
    private selectedRule: Rule;

    constructor() {
    }

    getRules(): RuleGroup[] {
        return (this.rules) ? [...this.rules] : [];
    }

    addRule(rule: Rule, ruleGroup: RuleGroup) {
        if (! this.rules) {
            this.initRules();
        }
        if (this.selectedRule) {
            this.updateRule(this.selectedRule, rule);
        } else {

        }
        this.onRulesChanged.next(this.rules);
    }

    addRuleGroup(ruleGroup: RuleGroup) {
        if (! this.rules) {
            this.initRules();
        }

        this.rules.push(ruleGroup);
        this.onRulesChanged.next(this.rules);
    }

    selectRule(rule: Rule) {
        this.selectedRule = rule;
    }

    private updateRule(oldRule: Rule, newRule: Rule) {
        for (let ruleGroup in this.rules) {
            this.rules[ruleGroup].updateRule(oldRule, newRule);
        }
    }

    private initRules() {
        this.rules = [];
    }
}
