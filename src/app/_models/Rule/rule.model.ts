import { Expression } from "../Expression/expression.model";
import { Operator } from "../Operator/operator.model";
import { Identifier } from '../Expression/Identifier/identifier.model';
import { Literal } from '../Expression/Literal/literal.model';
import { OperatorType } from '../Operator/OperatorList/OperatorType/operator-type.model';

export class Rule {
    id: string;
    private left: Expression;
    private operator: Operator;
    private right: Expression | null;
    valid: boolean;

    constructor(options?: object) {
        this.id = Math.random().toString(36).substr(2, 9);
        this.setLeft(
            (options && options.hasOwnProperty('left'))
                ? options['left']
                : null
        );

        this.setOperator(
            (options && options.hasOwnProperty('operator'))
                ? options['operator']
                : null
        );

        this.setRight(
            (options && options.hasOwnProperty('right'))
                ? options['right']
                : null
        );
    }

    setLeft(expression: Expression | null) {
        if (Rule.isCorrectType(expression, Expression)) {
            this.left = expression;
        } else {
            throw new Error(`The left side of a rule was expecting type Expression, but received ${ typeof expression }`);
        }
    }

    getLeft() {
        if (! this.left) {
            this.left = new Identifier('');
        }

        return this.left;
    }

    setOperator(operator: Operator) {
        if (Rule.isCorrectType(operator, Operator)) {
            this.operator = operator;
        } else {
            throw new Error(`The operator of a rule was expecting type Operator, but received ${ typeof operator }`);
        }
    }

    getOperator() {
        if (! this.operator) {
            this.operator = OperatorType.get('equals');
        }
        return this.operator;
    }

    setRight(expression: Expression) {
        if (Rule.isCorrectType(expression, Expression)) {
            this.right = expression;
        } else {
            throw new Error(`The right side of a rule was expecting type Expression, but received ${ typeof expression }`);
        }
    }

    getRight() {
        if (! this.right) {
            this.right = new Literal(null);
        }

        return this.right;
    }

    private static isCorrectType(value: any, type: any) {
        return ! (value !== null && ! (value instanceof type));
    }

    updateRule(oldRule: Rule, newRule: Rule) {
        this.left = newRule.left;
        this.operator = newRule.operator;
        this.right = newRule.right;
    }
}
