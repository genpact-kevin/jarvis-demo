import { TestBed } from '@angular/core/testing';

import { RuleService } from './rule.service';
import { RuleGroup } from "../RuleGroup/rule-group.model";
import { Rule } from "./rule.model";

describe('RuleServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RuleService = TestBed.get(RuleService);
    expect(service).toBeTruthy();
  });

  /** @test RuleService should allow new RuleGroups to be added
   * Given an existing set of Rules
   * When a new RuleGroup object is added
   * Then the new RuleGroup object should be visible in the rule list
   */
  it('should allow new RuleGroups to be added', () => {
    const service: RuleService = TestBed.get(RuleService);
    const ruleGroup = new RuleGroup();
    service.addRuleGroup(ruleGroup);
    const rules = service.getRules();

    expect(rules[1]).toBe(ruleGroup);
  });

  /** @test RuleService should allow new rules to be added to a RuleGroup
   * Given an existing list of RuleGroups
   * When a new Rule is added to a RuleGroup
   * Then the Rule should be visible in the rule list
   */
  it('should allow new rules to be added to a RuleGroup', () => {
    const service: RuleService = TestBed.get(RuleService);
    const ruleGroup = new RuleGroup();
    service.addRuleGroup(ruleGroup);
    const rule = new Rule();
    // service.addRule(rule);

    const rules = service.getRules();
  });
});
