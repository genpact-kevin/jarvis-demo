export class Operator {
    type = 'Operator';

    /**
     *
     * @param name
     * @param value
     * @param token
     * @param operands  An array of operand types mapped to HTML input types
     */
    constructor (
        public name: string,
        public value: number,
        public token: string,
        public operands: { type: string, suffix?: string, prefix?: string }[]
    ) {
    }
}
