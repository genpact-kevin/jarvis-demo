import { LogicalOperator } from "./logical-operator.model";

describe('LogicalOperator', () => {
  /** @test LogicalOperator should return correctly mapped values
   * Given an existing list of Operator types
   * And those Operator types are mapped via the constructor
   * When trying to access mapped values
   * Then the correct values should be returned
   */
  it('should return correctly mapped values', () => {
    expect(LogicalOperator.get('or').value).toBe(0x100);
    expect(LogicalOperator.get('||').value).toBe(0x100);
    expect(LogicalOperator.get('and').value).toBe(0x101);
    expect(LogicalOperator.get('&&').value).toBe(0x101);
  });

  /** @test LogicalOperator should return correctly mapped name
   * Given an existing list of Operator types
   * And those Operator types are mapped via the constructor
   * When trying to access mapped values
   * Then the correct names should be returned
   */
  it('should return correctly mapped name', () => {
    expect(LogicalOperator.get('or').name).toBe('or');
    expect(LogicalOperator.get('||').name).toBe('||');
    expect(LogicalOperator.get('and').name).toBe('and');
    expect(LogicalOperator.get('&&').name).toBe('&&');
  });

  /** @test LogicalOperator should throw an error if an unknown operator is passed to get
   * Given an existing list of Operator Types
   * When  an operator that is not on the list is passed to the get method
   * Then an error should be thrown indicating the operator is invalid
   */
  it('should throw an error if an unknown operator is passed to get', () => {
    expect(() => {
      LogicalOperator.get('@123')
    }).toThrow(new Error("Unknown operator '@123'"));
  });


  /** @test LogicalOperator can convert a value to a token
   * Given an existing list of Operator Types
   * When we get the operator token by value
   * Then we should see the correct operator token
   */
  it('can convert a value to a token', () => {
    expect(LogicalOperator.getToken(LogicalOperator.get('or').value)).toBe('valid if any of the following are true');
    expect(LogicalOperator.getToken(LogicalOperator.get('||').value)).toBe('valid if any of the following are true');
    expect(LogicalOperator.getToken(LogicalOperator.get('and').value)).toBe('valid if all of the following are true');
    expect(LogicalOperator.getToken(LogicalOperator.get('&&').value)).toBe('valid if all of the following are true');
  });
});

