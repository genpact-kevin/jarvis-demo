import { OperatorList } from "../operator-list.model";
import { Operator } from "../../operator.model";

export class LogicalOperator extends OperatorList {
  private static or = 0x100;
  private static and = 0x101;

  private static tokenMap = {
    0x100: 'valid if any of the following are true',
    0x101: 'valid if all of the following are true'
  };

  private static tokenToOperatorMap = {
    '||': new Operator('||', LogicalOperator.or, LogicalOperator.tokenMap[LogicalOperator.or], [{
      type: 'text'
    }]),
    'or': new Operator('or', LogicalOperator.or, LogicalOperator.tokenMap[LogicalOperator.or], [{
      type: 'text'
    }]),
    '&&': new Operator('&&', LogicalOperator.and, LogicalOperator.tokenMap[LogicalOperator.and], [{
      type: 'text'
    }]),
    'and': new Operator('and', LogicalOperator.and, LogicalOperator.tokenMap[LogicalOperator.and], [{
      type: 'text'
    }])
  };

  static get (type: string): Operator {
    if (LogicalOperator.tokenToOperatorMap.hasOwnProperty(type)) {
      return LogicalOperator.tokenToOperatorMap[type];
    }

    throw new Error(`Unknown operator '${type}'`);
  }

  static getToken(tokenValue: number) {
    if (LogicalOperator.tokenMap.hasOwnProperty(tokenValue)) {
      return LogicalOperator.tokenMap[tokenValue];
    }
  }
}
