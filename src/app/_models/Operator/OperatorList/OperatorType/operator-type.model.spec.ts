import { OperatorType } from "./operator-type.model";

describe('OperatorType', () => {
  /** @test OperatorType should return correctly mapped values
   * Given an existing list of Operator types
   * And those Operator types are mapped via the constructor
   * When trying to access mapped values
   * Then the correct values should be returned
   */
  it('should return correctly mapped values', () => {
    expect(OperatorType.get('!').value).toBe(0x00);
    expect(OperatorType.get('not').value).toBe(0x00);
    expect(OperatorType.get('==').value).toBe(0x01);
    expect(OperatorType.get('equals').value).toBe(0x01);
    expect(OperatorType.get('<=').value).toBe(0x04);
  });

  /** @test OperatorType should return correctly mapped name
   * Given an existing list of Operator types
   * And those Operator types are mapped via the constructor
   * When trying to access mapped values
   * Then the correct names should be returned
   */
  it('should return correctly mapped name', () => {
    expect(OperatorType.get('!').name).toBe('!');
    expect(OperatorType.get('not').name).toBe('not');
    expect(OperatorType.get('==').name).toBe('==');
    expect(OperatorType.get('equals').name).toBe('equals');
    expect(OperatorType.get('<=').name).toBe('<=');
  });

  /** @test OperatorType should throw an error if an unknown operator is passed to get
   * Given an existing list of Operator Types
   * When  an operator that is not on the list is passed to the get method
   * Then an error should be thrown indicating the operator is invalid
   */
  it('should throw an error if an unknown operator is passed to get', () => {
    expect(() => { OperatorType.get('@123') }).toThrow(new Error("Unknown operator '@123'"));
  });



  /** @test OperatorType can convert a value to a token
   * Given an existing list of Operator Types
   * When we get the operator token by value
   * Then we should see the correct operator token
   */
  it('can convert a value to a token', () => {
    expect(OperatorType.getToken(OperatorType.get('!').value)).toBe('is not');
    expect(OperatorType.getToken(OperatorType.get('not').value)).toBe('is not');
    expect(OperatorType.getToken(OperatorType.get('==').value)).toBe('is equal to');
    expect(OperatorType.getToken(OperatorType.get('equals').value)).toBe('is equal to');
    expect(OperatorType.getToken(OperatorType.get('<=').value)).toBe('is less than or equal to');
  });
});
