import { Operator } from "../../operator.model";
import { OperatorList } from "../operator-list.model";

export class OperatorType extends OperatorList {
    private static not = 0x00;
    private static equals = 0x01;
    private static notEquals = 0x02;
    private static lessThan = 0x03;
    private static lessThanOrEqual = 0x04;
    private static greaterThan = 0x05;
    private static greaterThanOrEqual = 0x06;

    private static tokenMap = {
        0x00: 'is not',
        0x01: 'is equal to',
        0x02: 'is not equal to',
        0x03: 'is less than',
        0x04: 'is less than or equal to',
        0x05: 'is greater than',
        0x06: 'is greater than or equal to',
    };

    private static tokenToOperatorMap = {
        '!': new Operator('!', OperatorType.not, OperatorType.tokenMap[OperatorType.not], [{
            type: 'text'
        }]),
        'not': new Operator('not', OperatorType.not, OperatorType.tokenMap[OperatorType.not], [{
            type: 'text'
        }]),
        '==': new Operator('==', OperatorType.equals, OperatorType.tokenMap[OperatorType.equals], [{
            type: 'text'
        }]),
        'equals': new Operator('equals', OperatorType.equals, OperatorType.tokenMap[OperatorType.equals], [{
            type: 'text'
        }]),
        '!=': new Operator('!=', OperatorType.notEquals, OperatorType.tokenMap[OperatorType.notEquals], [{
            type: 'text'
        }]),
        'notEquals': new Operator('notEquals', OperatorType.notEquals, OperatorType.tokenMap[OperatorType.notEquals], [{
            type: 'text'
        }]),
        '<': new Operator('<', OperatorType.lessThan, OperatorType.tokenMap[OperatorType.lessThan], [{
            type: 'number'
        }]),
        'lessThan': new Operator('lessThan', OperatorType.lessThan, OperatorType.tokenMap[OperatorType.lessThan], [{
            type: 'number'
        }]),
        '<=': new Operator('<=', OperatorType.lessThanOrEqual, OperatorType.tokenMap[OperatorType.lessThanOrEqual], [{
            type: 'number'
        }]),
        'lessThanOrEqual': new Operator('lessThanOrEqual', OperatorType.lessThanOrEqual, OperatorType.tokenMap[OperatorType.lessThanOrEqual], [{
            type: 'number'
        }]),
        '>': new Operator('>', OperatorType.greaterThan, OperatorType.tokenMap[OperatorType.greaterThan], [{
            type: 'number'
        }]),
        'greaterThan': new Operator('greaterThan', OperatorType.greaterThan, OperatorType.tokenMap[OperatorType.greaterThan], [{
            type: 'number'
        }]),
        '>=': new Operator('>=', OperatorType.greaterThanOrEqual, OperatorType.tokenMap[OperatorType.greaterThanOrEqual], [{
            type: 'number'
        }]),
        'greaterThanOrEqual': new Operator('greaterThanOrEqual', OperatorType.greaterThanOrEqual, OperatorType.tokenMap[OperatorType.greaterThanOrEqual], [{
            type: 'number'
        }]),
    };

    static get(type: string): Operator {
        if (OperatorType.tokenToOperatorMap.hasOwnProperty(type)) {
            return OperatorType.tokenToOperatorMap[type];
        }

        throw new Error(`Unknown operator '${ type }'`);
    }

    static getToken(tokenValue: number) {
        if (OperatorType.tokenMap.hasOwnProperty(tokenValue)) {
            return OperatorType.tokenMap[tokenValue];
        }
    }

    static getOperators(): Operator[] {
        return [
          new Operator('equals', OperatorType.equals, OperatorType.tokenMap[OperatorType.equals], [{
              type: 'number'
          }]),
          new Operator('notEquals', OperatorType.notEquals, OperatorType.tokenMap[OperatorType.notEquals], [{
              type: 'number'
          }]),
          new Operator('lessThan', OperatorType.lessThan, OperatorType.tokenMap[OperatorType.lessThan], [{
              type: 'number'
          }]),
          new Operator('lessThanOrEqual', OperatorType.lessThanOrEqual, OperatorType.tokenMap[OperatorType.lessThanOrEqual], [{
              type: 'number'
          }]),
          new Operator('greaterThan', OperatorType.greaterThan, OperatorType.tokenMap[OperatorType.greaterThan], [{
              type: 'number'
          }]),
          new Operator('greaterThanOrEqual', OperatorType.greaterThanOrEqual, OperatorType.tokenMap[OperatorType.greaterThanOrEqual], [{
              type: 'number'
          }]),
        ];
    }
}
