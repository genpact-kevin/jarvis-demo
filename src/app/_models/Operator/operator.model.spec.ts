import { Operator } from "./operator.model";

describe('Operator', () => {
  /** @test Operator can be initialized
   * When a new Operator object is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(new Operator('foo', 0x00, 'foo')).toBeTruthy();
  });

  /** @test Operator has a name
   * Given an existing operator
   * When we check the value of the property name
   * Then we should see the name of the operator
   */
  it('has a name', () => {
    const operator = new Operator('foo', 0x00, 'foo');
    expect(operator.name).toBe('foo');
  });

  /** @test Operator has a value
   * Given an existing operator
   * When we check the value of the property value
   * Then we should see the value of the operator
   */
  it('has a value', () => {
    const operator = new Operator('foo', 0x00, 'foo');
    expect(operator.value).toBe(0x00);
  });
});
