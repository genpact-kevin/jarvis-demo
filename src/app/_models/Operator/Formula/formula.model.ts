export class Formula {
    type = 'Function';

    constructor (
        public name: string,
        public value: number,
        public token: string,
        public parameters: {type: string, suffix?: string, prefix?: string}[]
    ) {}
}
