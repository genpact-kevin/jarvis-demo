import { RuleGroup } from "./rule-group.model";
import { Rule } from "../Rule/rule.model";
import { Identifier } from "../Expression/Identifier/identifier.model";
import { OperatorType } from "../Operator/OperatorList/OperatorType/operator-type.model";
import { Literal } from "../Expression/Literal/literal.model";
import { LogicalOperator } from "../Operator/OperatorList/LogicalOperator/logical-operator.model";

describe('RuleGroup', () => {
  /** @test RuleGroup can be initialized
   * Given
   * When a new RuleGroup object is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(new RuleGroup()).toBeTruthy();
  });

  /** @test RuleGroup can have rules added
   * Given an existing RuleGroup
   * And two existing Rule objects
   * When the Rule objects are added to the RuleGroup
   * Then the length of the RuleObject should be 2
   */
  it('can have rules added', () => {
    const ruleGroup = new RuleGroup();
    const rule1 = new Rule({
      left: new Identifier('foo'),
      operator: OperatorType.get('>='),
      right: new Literal(7)
    });
    const rule2 = new Rule({
      left: new Identifier('foo'),
      operator: OperatorType.get('>='),
      right: new Literal(7)
    });

    ruleGroup.setRules([
      rule1,
      rule2
    ]);

    expect(ruleGroup.length).toBe(2);
  });

  /** @test RuleGroup can have rules added with constructor
   * Given 2 existing Rule objects
   * When the Rule objects are passed to the RuleGroup constructor
   * Then the length of the RuleGroup should be 2
   */
  it('can have rules added with constructor', () => {
    const rule1 = new Rule({
      left: new Identifier('foo'),
      operator: OperatorType.get('>='),
      right: new Literal(7)
    });
    const rule2 = new Rule({
      left: new Identifier('foo'),
      operator: OperatorType.get('>='),
      right: new Literal(7)
    });
    const ruleGroup = new RuleGroup({
      rules: [
        rule1,
        rule2
      ]
    });

    expect(ruleGroup.length).toBe(2);
  });

  /** @test RuleGroup can add rules to an existing set of rules
   * Given an existing RuleGroup object with existing rules
   * And and existing Rule object
   * When the other Rule is added to the RuleGroup
   * Then the length of RuleGroup should change from 1 to 2
   */
  it('can add rules to an existing set of rules', () => {
    const ruleGroup = new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        })
      ]
    });
    const rule = new Rule({
      left: new Identifier('foo'),
      operator: OperatorType.get('>='),
      right: new Literal(7)
    });

    expect(ruleGroup.length).toBe(1);

    ruleGroup.addRule(rule);

    expect(ruleGroup.length).toBe(2);
  });

  /** @test Rule-groupModel can have rules deleted
   * Given an existing RuleGroup
   * With 2 existing Rule objects
   * When the Rule object with an index of of 1 is deleted
   * Then the length of RuleGroup should change from 2 to 1
   */
  it('can have rules deleted', () => {
    const ruleGroup = new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        }),
        new Rule({
          left: new Identifier('foo'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        })
      ]
    });

    expect(ruleGroup.length).toBe(2);

    ruleGroup.deleteRule(1);

    expect(ruleGroup.length).toBe(1);
  });

  /** @test Rule-groupModel can reorder rules
   * Given an existing RuleGroup
   * With 5 existing rules
   * When we move rules
   * Then the order of the rules should change
   */
  it('can reorder rules', () => {
    const ruleGroup = new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo0'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        }),
        new Rule({
          left: new Identifier('foo1'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        }),
        new Rule({
          left: new Identifier('foo2'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        }),
        new Rule({
          left: new Identifier('foo3'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        }),
        new Rule({
          left: new Identifier('foo4'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        })
      ]
    });

    expect((<Identifier> (<Rule> ruleGroup.rules[0]).getLeft()).name).toBe('foo0');
    expect((<Identifier> (<Rule> ruleGroup.rules[3]).getLeft()).name).toBe('foo3');

    ruleGroup.moveRule(0, 3);

    expect((<Identifier> (<Rule> ruleGroup.rules[0]).getLeft()).name).toBe('foo1');
    expect((<Identifier> (<Rule> ruleGroup.rules[3]).getLeft()).name).toBe('foo0');
  });

  /** @test RuleGroup should have a logic operator
   * Given an existing Operator
   * When the operator property is set on a new RuleGroup object
   * Then the operator should be visible on the RuleGroup object
   */
  it('should have a logic operator', () => {
    const operator = LogicalOperator.get('&&');
    const ruleGroup = new RuleGroup({
      rules: [
      new Rule({
        left: new Identifier('foo'),
        operator: OperatorType.get('>='),
        right: new Literal(7)
      })],
      operator: operator
    });

    expect(ruleGroup.operator).toEqual(operator);
  });

  /** @test RuleGroup can have RuleGroup objects added to rules
   * Given two existing RuleGroup objects
   * When one RuleGroup object is added to the other RuleGroup object's rules
   * Then the added RuleGroup object should be visible in the rule array
   */
  it('can have RuleGroup objects added to rules', () => {
    const ruleGroup1 = new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        })],
      operator: LogicalOperator.get('&&')
    });
    const ruleGroup2 = new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        })],
      operator: LogicalOperator.get('&&')
    });

    ruleGroup1.addRule(ruleGroup2);

    expect(ruleGroup1.rules[1]).toEqual(ruleGroup2);
  });

  /** @test RuleGroup should have a consequent
   * Given an existing RuleGroup
   * When we check the consequent
   * Then we should see the consequent value
   */
  it('should have a consequent', () => {
    const ruleGroup = new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        })
      ],
      consequent: true
    });
    expect(ruleGroup.consequent).toBeTruthy();
  });

  /** @test RuleGroup should have a condition
   * Given an existing RuleGroup
   * When we check the condition
   * Then we should see the condition value
   */
  it('should have a condition', () => {
    const ruleGroup = new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo'),
          operator: OperatorType.get('>='),
          right: new Literal(7)
        })
      ],
      condition: true
    });
    expect(ruleGroup.condition).toBeTruthy();
  });
});
