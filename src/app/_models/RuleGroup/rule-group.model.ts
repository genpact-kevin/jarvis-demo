import { Rule } from "../Rule/rule.model";
import { Operator } from "../Operator/operator.model";
import { LogicalOperator } from "../Operator/OperatorList/LogicalOperator/logical-operator.model";

export class RuleGroup {
  id: string;
  type = "RuleGroup";
  rules: (Rule | RuleGroup)[];
  operator: Operator;
  consequent: boolean;
  condition: boolean;
  valid: boolean;
  deleted: boolean = false;

  constructor (options?: {}) {
    this.id = Math.random().toString(36).substr(2, 9);
    this.consequent = true;
    this.condition = true;
    this.setOperator(LogicalOperator.get('&&'));
    this.rules = [];

    if (options) {
      if (options.hasOwnProperty('rules')) {
        this.setRules(options['rules']);
      }

      if (options.hasOwnProperty('operator')) {
        this.setOperator(options['operator']);
      }

      if (options.hasOwnProperty('consequent')) {
        this.consequent = options['consequent'];
      }

      if (options.hasOwnProperty('condition')) {
        this.condition = options['condition'];
      }
    }
  }

  get length(): number {
    return this.rules.length;
  };

  setRules(rules: (Rule | RuleGroup)[]) {
    this.rules = rules;
  }

  setOperator(operator: Operator) {
    this.operator = operator;
  }

  addRule(rule: Rule | RuleGroup) {
    this.rules.push(rule);
  }

  pushRule(rule: Rule | RuleGroup) {
    this.addRule(rule);
  }

  deleteRule(index: number) {
    this.deleted = true;
  }

  moveRule(currentIndex: number, newIndex: number) {
    const rule = this.rules.splice(currentIndex, 1);
    this.rules.splice(newIndex, 0, rule[0]);
  }

  updateRule(oldRule: Rule, newRule: Rule) {
    for (let index in this.rules) {
      if (this.rules[index] instanceof RuleGroup) {
        if (this.rules[index].hasOwnProperty('updateRule')) {
          this.rules[index].updateRule(oldRule, newRule);
        }
      }

      if (this.rules[index].id === oldRule.id) {
        this.rules[index] = newRule;
      }
    }
  }
}
