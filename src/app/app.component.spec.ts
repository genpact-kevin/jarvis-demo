import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { RuleEditorComponent } from "./rule-editor/rule-editor.component";
import { RuleGroupComponent } from "./rule-editor/rule-group/rule-group.component";
import { RuleComponent } from "./rule-editor/rule-group/rule/rule.component";
import { ExpressionComponent } from "./rule-editor/expression/expression.component";
import { IdentifierComponent } from "./rule-editor/expression/identifier/identifier.component";
import { LiteralComponent } from "./rule-editor/expression/literal/literal.component";
import { OperatorComponent } from "./rule-editor/operator/operator.component";
import { EditRuleComponent } from "./rule-editor/edit-rule/edit-rule.component";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        RuleEditorComponent,
        RuleGroupComponent,
        RuleComponent,
        ExpressionComponent,
        IdentifierComponent,
        LiteralComponent,
        OperatorComponent,
        EditRuleComponent
      ],
    }).compileComponents();
  }));

  /** @test AppComponent should create the app
   * When the AppComponent is initialized
   * Then the Angular application should be created
   */
  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
