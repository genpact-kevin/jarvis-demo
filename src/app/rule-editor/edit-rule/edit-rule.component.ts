import { Component, OnDestroy, OnInit } from '@angular/core';
import { Literal } from "../../_models/Expression/Literal/literal.model";
import { Identifier } from "../../_models/Expression/Identifier/identifier.model";
import { Operator } from "../../_models/Operator/operator.model";
import { Expression } from "../../_models/Expression/expression.model";
import { Rule } from "../../_models/Rule/rule.model";
import { RuleService } from "../../_models/Rule/rule.service";

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-rule.component.html',
  styleUrls: ['./edit-rule.component.scss']
})
export class EditRuleComponent implements OnInit, OnDestroy {
  ruleData: Rule;
  private readonly ruleComponents = {
    variables: {
      variable: new Identifier('isActive'),
      valid: new Literal(true),
      boolean: new Literal(true)
    },
    statements: {},
    conditions: {
      isEmpty: new Operator('!!', 0x07, 'is empty', [{
        type: 'text'
      }]),
      isNotEmpty: new Operator('!', 0x08, 'is not empty', [{
        type: 'text'
      }])
    },
    formulas: {}
  };

  constructor(private ruleService: RuleService) {
  }

  ngOnInit() {
    this.ruleData = new Rule();
  }

  ngOnDestroy(): void {
  }

  addToRule(component: Expression | Operator) {
    if (component instanceof Expression) {
      if (!this.ruleData.getLeft()) {
        this.ruleData.setLeft(component);
      } else if (!this.ruleData.getRight()) {
        this.ruleData.setRight(component);
      }
    }

    if (component instanceof Operator) {
      if (!this.ruleData.getOperator()) {
        this.ruleData.setOperator(component);
      }
    }
  }

  addRule() {
    // this.ruleService.addRule(this.ruleData);
  }
}
