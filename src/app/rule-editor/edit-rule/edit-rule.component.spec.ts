import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRuleComponent } from './edit-rule.component';
import { RuleComponent } from "../rule-group/rule/rule.component";
import { ExpressionComponent } from "../expression/expression.component";
import { OperatorComponent } from "../operator/operator.component";
import { IdentifierComponent } from "../expression/identifier/identifier.component";
import { LiteralComponent } from "../expression/literal/literal.component";

describe('EditModalComponent', () => {
  let component: EditRuleComponent;
  let fixture: ComponentFixture<EditRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EditRuleComponent,
        RuleComponent,
        ExpressionComponent,
        IdentifierComponent,
        LiteralComponent,
        OperatorComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
