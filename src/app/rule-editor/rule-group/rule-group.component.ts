import { Component, Input, OnInit } from '@angular/core';
import { RuleGroup } from "../../_models/RuleGroup/rule-group.model";
import { Rule } from "../../_models/Rule/rule.model";
import { LogicalOperator } from "../../_models/Operator/OperatorList/LogicalOperator/logical-operator.model";
import { RuleService } from "../../_models/Rule/rule.service";

@Component({
  selector: 'app-rule-group',
  templateUrl: './rule-group.component.html',
  styleUrls: ['./rule-group.component.scss']
})
export class RuleGroupComponent implements OnInit {
  @Input() ruleGroupData: RuleGroup;
  deleted: boolean = false;

  constructor(private ruleService: RuleService) {
  }

  ngOnInit() {
  }

  isRule(rule: Rule | RuleGroup) {
    return (rule instanceof Rule);
  }

  isAnd() {
    return (this.ruleGroupData.operator === LogicalOperator.get('&&'));
  }

  getConsequentString(consequent: boolean) {
    return (consequent) ? 'valid' : 'invalid';
  }

  addRule() {
    this.ruleGroupData.addRule(new Rule());
  }

  addRuleGroup() {
    this.ruleGroupData.addRule(new RuleGroup());
  }

  deleteRule(rule: Rule | RuleGroup) {

  }

  delete() {
    this.deleted = true;
  }
}
