import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RuleComponent } from './rule.component';
import { ExpressionComponent } from "../../expression/expression.component";
import { IdentifierComponent } from "../../expression/identifier/identifier.component";
import { LiteralComponent } from "../../expression/literal/literal.component";
import { OperatorComponent } from "../../operator/operator.component";
import { Rule } from "../../../_models/Rule/rule.model";
import { Identifier } from "../../../_models/Expression/Identifier/identifier.model";
import { OperatorType } from "../../../_models/Operator/OperatorList/OperatorType/operator-type.model";
import { Operator } from "../../../_models/Operator/operator.model";
import { Literal } from "../../../_models/Expression/Literal/literal.model";
import { RuleGroupComponent } from "../rule-group.component";

describe('RuleComponent', () => {
  let component: RuleComponent;
  let fixture: ComponentFixture<RuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RuleComponent,
        RuleGroupComponent,
        ExpressionComponent,
        IdentifierComponent,
        LiteralComponent,
        OperatorComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleComponent);
    component = fixture.componentInstance;
    component.ruleData = new Rule({
      left: new Identifier('foo'),
      operator: new Operator('foo', 0x00, 'foo'),
      right: new Literal('bar')
    });
    fixture.detectChanges();
  });

  /** @test RuleComponent can be initialized
   * When a RuleComponent is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(component).toBeTruthy();
  });
});
