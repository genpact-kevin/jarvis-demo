import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Rule } from "../../../_models/Rule/rule.model";
import { RuleService } from '../../../_models/Rule/rule.service';
import { ActivatedRoute, Params } from '@angular/router';
import { SchemaService } from '../../../common/schema.service';
import { Identifier } from '../../../_models/Expression/Identifier/identifier.model';
import { Operator } from '../../../_models/Operator/operator.model';
import { Subject, Subscription } from 'rxjs';
import { Formula } from '../../../_models/Operator/Formula/formula.model';

@Component({
    selector: 'app-rule',
    templateUrl: './rule.component.html',
    styleUrls: ['./rule.component.scss']
})
export class RuleComponent implements OnInit, OnDestroy {
    @Input() ruleData: Rule;
    @Output() deleteRule: EventEmitter<null> = new EventEmitter();

    onIdentifierChanged = new Subject<Identifier>();
    onIdentifierChangedSubscription: Subscription;
    onOperatorsChanged = new Subject<Operator[]>();
    onOperatorsChangedSubscription: Subscription;
    onOperatorChanged = new Subject<Operator>();
    onOperatorChangedSubscription: Subscription;

    identifiers: Identifier[];
    identifier: Identifier;
    operators: Operator[];
    operator: Operator;

    deleted: boolean = false;
    property: string;

    identifierOptionsOpen = false;

    // Move to operators.service.ts
    nullOperators = [
        new Operator('empty', 0x0, 'is empty', []),
        new Operator('notEmpty', 0x0, 'is not empty', []),
        new Operator('null', 0x0, 'is null', []),
        new Operator('notNull', 0x0, 'is not null', []),
        new Operator('undefined', 0x0, 'is undefined', []),
        new Operator('notUndefined', 0x0, 'is defined', []),
    ];

    booleanOperators = [
        new Operator('true', 0x0, 'is true', []),
        new Operator('false', 0x0, 'is false', []),
        new Operator('notTrue', 0x0, 'is not true', []),
        new Operator('notFalse', 0x0, 'is not false', []),
    ];

    numberOperators = [
        new Operator('equal', 0x0, 'is equal to', [
            {type: 'number'}
        ]),
        new Operator('notEqual', 0x0, 'is not equal to', [
            {type: 'number'}
        ]),
        new Operator('greaterThan', 0x0, 'is greater than', [
            {type: 'number'}
        ]),
        new Operator('greaterThanOrEqual', 0x0, 'is greater than or equal to', [
            {type: 'number'}
        ]),
        new Operator('lessThan', 0x0, 'is less than', [
            {type: 'number'}
        ]),
        new Operator('lessThanOrEqual', 0x0, 'is less than or equal to', [
            {type: 'number'}
        ]),
        new Operator('between', 0x0, 'is between', [
            {type: 'number', suffix: 'and'},
            {type: 'number'}
        ]),
        new Operator('notBetween', 0x0, 'is not between', [
            {type: 'number', suffix: 'and'},
            {type: 'number'}
        ]),
    ];

    stringOperators = [
        new Operator('contains', 0x0, 'contains', [{type: 'text'}]),
        new Operator('notContains', 0x0, 'does not contain', [{type: 'text'}]),
        new Operator('begins', 0x0, 'begins with', [{type: 'text'}]),
        new Operator('ends', 0x0, 'ends with', [{type: 'text'}]),
        new Operator('exactly', 0x0, 'is exactly', [{type: 'text'}]),
    ];

    dateOperators = [
        new Operator('isDate', 0x0, 'is', [{type: 'date'}]),
        new Operator('isBeforeDate', 0x0, 'is before', [{type: 'date'}]),
        new Operator('isAfterDate', 0x0, 'is after', [{type: 'date'}]),
        new Operator('isBetweenDates', 0x0, 'is between', [
            {type: 'date', suffix: 'and'},
            {type: 'date'}
        ]),
    ];

    arrayOperators = [
        new Operator('contains', 0x0, 'contains', [{type: 'text'}]),
        new Operator('notContains', 0x0, 'does not contain', [{type: 'text'}]),
    ];

    operatorTypes = {
        'integer': [...this.numberOperators, ...this.nullOperators],
        'long': [...this.numberOperators, ...this.nullOperators],
        'boolean': [...this.booleanOperators, ...this.nullOperators],
        'string': [...this.stringOperators, ...this.nullOperators],
        'date': [...this.dateOperators, ...this.nullOperators],
        'json_array': [...this.arrayOperators, ...this.nullOperators],
        'json_object': [...this.nullOperators]
    };

    formulas = [
        new Formula('count', 0x0, 'number of elements in', [
            {type: 'identifier'}
        ]),
        new Formula('find ... in', 0x0, 'find', [
            {type: 'text', suffix: 'in'},
            {type: 'identifier'}
        ]),
        new Formula('get element ... in', 0x0, 'get element', [
            {type: 'number', suffix: 'in'},
            {type: 'identifier'}
        ]),
        new Formula('get elements ... where', 0x0, 'get elements from', [
            {type: 'identifier', suffix: 'where'},
            {type: 'expression'}
        ]),
    ];

    constructor (
        private ruleService: RuleService,
        private activatedRoute: ActivatedRoute,
        private schemaService: SchemaService
    ) {
    }

    ngOnInit () {
        this.identifiers = this.schemaService.getIdentifiers();

        this.activatedRoute.params.subscribe((params: Params) => {
            this.property = params['property'];
            this.identifier = this.getIdentifier(this.property);
            this.operators = this.operatorTypes[this.identifier.dataType];
            this.operator = this.operators[0];
        });

        this.onIdentifierChangedSubscription = this.onIdentifierChanged
            .subscribe((identifier: Identifier) => {
                this.identifier = identifier;
                this.onOperatorsChanged.next(this.operatorTypes[identifier.dataType]);
            });

        this.onOperatorsChangedSubscription = this.onOperatorsChanged
            .subscribe((operators: Operator[]) => {
                this.operators = operators;
                this.onOperatorChanged.next(operators[0]);
            });

        this.onOperatorChangedSubscription = this.onOperatorChanged
            .subscribe((operator: Operator) => {
                this.operator = operator;
            });
    }

    ngOnDestroy (): void {
        this.onIdentifierChangedSubscription.unsubscribe();
        this.onOperatorsChangedSubscription.unsubscribe();
        this.onOperatorChangedSubscription.unsubscribe();
    }

    delete () {
        this.deleted = true;
    }

    identifierChanged ($event: Event) {
        console.log((<HTMLSelectElement>$event.target).value);
        const identifierName = (<HTMLSelectElement>$event.target).value;
        this.onIdentifierChanged.next(this.getIdentifier(identifierName));
    }

    operatorChanged (operator: Operator) {
        this.onOperatorChanged.next(operator);
    }

    private getIdentifier (property: string): Identifier {
        return this.identifiers.filter((identifier) => {
            return (identifier.name === property);
        })[0];
    }

    getOperandCountArray (operandCount: number) {
        return Array(operandCount).fill(1);
    }
}
