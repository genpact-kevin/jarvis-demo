import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RuleGroupComponent } from './rule-group.component';
import { RuleComponent } from "./rule/rule.component";
import { IdentifierComponent } from "../expression/identifier/identifier.component";
import { LiteralComponent } from "../expression/literal/literal.component";
import { ExpressionComponent } from "../expression/expression.component";
import { OperatorComponent } from "../operator/operator.component";
import { RuleGroup } from "../../_models/RuleGroup/rule-group.model";
import { Rule } from "../../_models/Rule/rule.model";
import { Identifier } from "../../_models/Expression/Identifier/identifier.model";
import { Operator } from "../../_models/Operator/operator.model";
import { Literal } from "../../_models/Expression/Literal/literal.model";

describe('RuleGroupComponent', () => {
  let component: RuleGroupComponent;
  let fixture: ComponentFixture<RuleGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RuleGroupComponent,
        RuleComponent,
        ExpressionComponent,
        IdentifierComponent,
        LiteralComponent,
        OperatorComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleGroupComponent);
    component = fixture.componentInstance;
    component.ruleGroupData = new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo'),
          operator: new Operator('foo', 0x00, 'foo'),
          right: new Literal('bar')
        })
      ]
    });
    fixture.detectChanges();
  });

  /** @test Rule-groupComponent can be initialized
   * When a RuleGroupComponent is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(component).toBeTruthy();
  });
});
