import { Component, Input, OnInit } from '@angular/core';
import { OperatorType } from "../../_models/Operator/OperatorList/OperatorType/operator-type.model";
import { Operator } from "../../_models/Operator/operator.model";

@Component({
  selector: 'app-operator',
  templateUrl: './operator.component.html',
  styleUrls: ['./operator.component.scss']
})
export class OperatorComponent implements OnInit {
  @Input() operatorData: Operator | OperatorType | null;
  operators: Operator[];

  constructor() {
  }

  ngOnInit() {
    this.operators = OperatorType.getOperators();
  }
}
