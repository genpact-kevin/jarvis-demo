import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OperatorComponent } from './operator.component';
import { Operator } from "../../_models/Operator/operator.model";

describe('OperatorComponent', () => {
  let component: OperatorComponent;
  let fixture: ComponentFixture<OperatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OperatorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorComponent);
    component = fixture.componentInstance;
    component.operatorData = new Operator('foo', 0x00, 'foo');
    fixture.detectChanges();
  });

  /** @test OperatorComponent can be initialized
   * When an OperatorComponent is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(component).toBeTruthy();
  });
});
