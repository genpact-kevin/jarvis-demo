import { Component, OnInit } from '@angular/core';
import { RuleGroup } from "../_models/RuleGroup/rule-group.model";
import { Rule } from "../_models/Rule/rule.model";
import { Identifier } from "../_models/Expression/Identifier/identifier.model";
import { OperatorType } from "../_models/Operator/OperatorList/OperatorType/operator-type.model";
import { Literal } from "../_models/Expression/Literal/literal.model";
import { RuleService } from "../_models/Rule/rule.service";
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-rule-editor',
  templateUrl: './rule-editor.component.html',
  styleUrls: ['./rule-editor.component.scss']
})
export class RuleEditorComponent implements OnInit {
  ruleData: RuleGroup[];
  showEditModal = false;
  selectedRule: Rule;
  object: string;
  property: string;

  constructor(
      private ruleService: RuleService,
      private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.object = params['object'];
      this.property = params['property'];
    });

    document.addEventListener('showEditModal', () => {
      if (!this.showEditModal) {
        this.showEditModal = true;
      }
    });

    document.addEventListener('hideEditModal', () => {
      if (this.showEditModal) {
        this.showEditModal = false;
      }
    });

    this.ruleService.onRulesChanged.subscribe((rules: RuleGroup[]) => {
      this.ruleData = this.ruleService.getRules();
    });
    this.ruleData = this.ruleService.getRules();
  }

  addRuleGroup() {
    this.ruleService.addRuleGroup(new RuleGroup());
  }
}
