import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IdentifierComponent } from './identifier.component';
import { Identifier } from "../../../_models/Expression/Identifier/identifier.model";

describe('IdentifierComponent', () => {
  let component: IdentifierComponent;
  let fixture: ComponentFixture<IdentifierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IdentifierComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentifierComponent);
    component = fixture.componentInstance;
    component.identifierData = new Identifier('foo');
    fixture.detectChanges();
  });

  /** @test IdentifierComponent can be initialized
   * When IdentifierComponent is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(component).toBeTruthy();
  });
});
