import { Component, Input, OnInit } from '@angular/core';
import { Expression } from "../../../_models/Expression/expression.model";
import { Identifier } from "../../../_models/Expression/Identifier/identifier.model";
import { SchemaService } from '../../../common/schema.service';

@Component({
  selector: 'app-identifier',
  templateUrl: './identifier.component.html',
  styleUrls: ['./identifier.component.scss']
})
export class IdentifierComponent implements OnInit {
  @Input() identifierData: Expression | Identifier | null;
    identifiers: Identifier[];

  constructor(private schemaService: SchemaService) {
  }

  ngOnInit() {
    this.identifiers = this.schemaService.getIdentifiers();
  }
}
