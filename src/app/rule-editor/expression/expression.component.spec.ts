import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExpressionComponent } from './expression.component';
import { Literal } from "../../_models/Expression/Literal/literal.model";
import { IdentifierComponent } from "./identifier/identifier.component";
import { LiteralComponent } from "./literal/literal.component";

describe('ExpressionComponent', () => {
  let component: ExpressionComponent;
  let fixture: ComponentFixture<ExpressionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ExpressionComponent,
        IdentifierComponent,
        LiteralComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpressionComponent);
    component = fixture.componentInstance;
    component.expressionData = new Literal('foo');
    fixture.detectChanges();
  });

  /** @test ExpressionComponent can be initialized
   * When an ExpressionComponent is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(component).toBeTruthy();
  });
});
