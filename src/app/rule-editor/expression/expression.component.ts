import { Component, Input, OnInit } from '@angular/core';
import { Expression } from "../../_models/Expression/expression.model";
import { Identifier } from "../../_models/Expression/Identifier/identifier.model";
import { Literal } from "../../_models/Expression/Literal/literal.model";

@Component({
  selector: 'app-expression',
  templateUrl: './expression.component.html',
  styleUrls: ['./expression.component.scss']
})
export class ExpressionComponent implements OnInit {
  @Input() expressionData: Expression | Identifier | Literal | null;

  constructor() {
  }

  ngOnInit() {
  }
}
