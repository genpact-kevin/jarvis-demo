import { Component, Input, OnInit } from '@angular/core';
import { Literal } from "../../../_models/Expression/Literal/literal.model";

@Component({
  selector: 'app-literal',
  templateUrl: './literal.component.html',
  styleUrls: ['./literal.component.scss']
})
export class LiteralComponent implements OnInit {
  @Input() literalData: Literal;

  constructor() {
  }

  ngOnInit() {
  }
}
