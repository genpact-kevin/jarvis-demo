import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LiteralComponent } from './literal.component';
import { Literal } from "../../../_models/Expression/Literal/literal.model";

describe('LiteralComponent', () => {
  let component: LiteralComponent;
  let fixture: ComponentFixture<LiteralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LiteralComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiteralComponent);
    component = fixture.componentInstance;
    component.literalData = new Literal('foo');
    fixture.detectChanges();
  });

  /** @test LiteralComponent can be initialized
   * Given
   * When a LiteralComponent is initialized
   * Then it should exist
   */
  it('can be initialized', () => {
    expect(component).toBeTruthy();
  });
});
