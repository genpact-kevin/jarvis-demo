import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RuleEditorComponent } from './rule-editor.component';
import { RuleGroup } from "../_models/RuleGroup/rule-group.model";
import { Rule } from "../_models/Rule/rule.model";
import { Identifier } from "../_models/Expression/Identifier/identifier.model";
import { Operator } from "../_models/Operator/operator.model";
import { Literal } from "../_models/Expression/Literal/literal.model";
import { RuleComponent } from "./rule-group/rule/rule.component";
import { ExpressionComponent } from "./expression/expression.component";
import { IdentifierComponent } from "./expression/identifier/identifier.component";
import { LiteralComponent } from "./expression/literal/literal.component";
import { OperatorComponent } from "./operator/operator.component";
import { RuleGroupComponent } from "./rule-group/rule-group.component";
import { EditRuleComponent } from "./edit-rule/edit-rule.component";

describe('RuleEditorComponent', () => {
  let component: RuleEditorComponent;
  let fixture: ComponentFixture<RuleEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RuleEditorComponent,
        RuleGroupComponent,
        RuleComponent,
        ExpressionComponent,
        IdentifierComponent,
        LiteralComponent,
        OperatorComponent,
        EditRuleComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleEditorComponent);
    component = fixture.componentInstance;
    component.ruleData = [new RuleGroup({
      rules: [
        new Rule({
          left: new Identifier('foo'),
          operator: new Operator('foo', 0x00, 'foo'),
          right: new Literal('bar')
        })
      ]
    })];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
