import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-data-management',
    templateUrl: './data-management.component.html',
    styleUrls: ['./data-management.component.scss']
})
export class DataManagementComponent implements OnInit {
    dataObjectsData = [
        ["accounts", "", 65, "", ""],
        ["documents", "", 11, "", ""],
        ["entities", "", 174, "", ""],
        ["items", "", 730, "", ""],
        ["transactions", "", 12, "", ""],
        ["abcd", "jhjhjnngnhgvnh", 3, "", ""],
        ["AcademicCollocationList", "AcademicCollocationList", 2, "", ""],
        ["Address", "", 11, "", ""],
        ["ADDRESS2", "Property Description", 2, "", ""],
        ["AmerisACH", "", 0, "", ""],
        ["AmerisTransactions", "", 0, "", ""],
        ["AmerisWires", "Wires transaction for Ameris", 0, "", ""],
        ["bsjhafb", "qsefqe", 0, "", ""],
        ["Copy of abcd", "asdfgh", 0, "", ""],
        ["Copy of bsjhafb", "asdf", 0, "", ""],
        ["Copy of may1", "", 0, "", ""],
        ["Copy of test101", "", 0, "", ""],
        ["cust details", "basic cust details", 0, "", ""],
        ["Customer", "Customer record for KYC", 0, "", ""],
        ["CustomerExtension", "Demo extension of the entity structure ", 0, "", ""],
        ["DateOfBirthGeneGenerator", "DOB to age", 0, "", ""],
        ["DQEntites", "Object to support DQ Effort", 0, "", ""],
        ["Email", "", 0, "", ""],
        ["empdetail", "test description", 0, "", ""],
        ["entities2", "", 0, "", ""],
        ["EntitiesExtended", "Just an entities object extended from core", 0, "", ""],
        ["EX", "Property Description", 0, "", ""],
        ["Extended object", "updated", 0, "", ""],
        ["Extended Transaction Test", "Test Transaction", 0, "", ""],
        ["Extended Transaction", "Extended Transaction Description", 0, "", ""],
        ["ID2", "Property Description", 0, "", ""],
        ["Identification Documents", "", 0, "", ""],
        ["Industry Types", "", 0, "", ""],
        ["JohnsDataObject2", "Test object for John.", 0, "", ""],
        ["LongFieldTransactions", "For testing, uses long field names. This is a test.", 0, "", ""],
        ["may0101", "", 0, "", ""],
        ["may1", "", 0, "", ""],
        ["nctest", "bnchcnhsy", 0, "", ""],
        ["newentities", "new entities extends from the entities.", 0, "", ""],
        ["Nicks Wacky Transactions", "", 0, "", ""],
        ["Nobject1", "test object", 0, "", ""],
        ["nvgc", "f", 0, "", ""],
        ["obj1", "T1", 0, "", ""],
        ["Phones", "", 0, "", ""],
        ["sampleObject", "sampleObject", 0, "", ""],
        ["sampleObject1", "sample object d", 0, "", ""],
        ["sampleObject2", "sampleObject2", 0, "", ""],
        ["Test Extended Transactions", "", 0, "", ""],
        ["test", "qwrwwerwe", 0, "", ""],
        ["test101", "", 0, "", ""],
        ["test1234", "testing description", 0, "", ""],
        ["test29", "111", 0, "", ""],
        ["test30", "test30", 0, "", ""],
        ["test301", "", 0, "", ""],
        ["testing123", "testing", 0, "", ""],
        ["testNC", "kgjhjh", 0, "", ""],
        ["Weblink", "", 0, "", ""]
    ];

    constructor() {
    }

    ngOnInit() {
    }

}
