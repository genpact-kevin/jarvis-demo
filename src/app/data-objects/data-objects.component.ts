import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-data-objects',
    templateUrl: './data-objects.component.html',
    styleUrls: ['./data-objects.component.scss']
})
export class DataObjectsComponent implements OnInit {
    objectProperties = [
        {
            "display_label": "entityId",
            "pretty_name": "entityId",
            "is_object_array": false,
            "json_key": "entityId",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ENTID",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "long",
            "column_type": "static"
        },
        {
            "display_label": "entityType",
            "pretty_name": "entityType",
            "is_object_array": false,
            "json_key": "entityType",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ENTTYP",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "isActive",
            "pretty_name": "isActive",
            "is_object_array": false,
            "json_key": "isActive",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ISACTV",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "onboardingDate",
            "pretty_name": "onboardingDate",
            "is_object_array": false,
            "json_key": "onboardingDate",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ONBDT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "date",
            "column_type": "static"
        },
        {
            "display_label": "occupation",
            "pretty_name": "occupation",
            "is_object_array": false,
            "json_key": "occupation",
            "column_family": "c",
            "isReportable": true,
            "column_name": "OCCUP",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "sourceOfIncome",
            "pretty_name": "sourceOfIncome",
            "is_object_array": false,
            "json_key": "sourceOfIncome",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SRCOFINC",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "taxIdNumber",
            "pretty_name": "taxIdNumber",
            "is_object_array": false,
            "json_key": "taxIdNumber",
            "column_family": "c",
            "isReportable": true,
            "column_name": "TAXIDNO",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": true,
                "validationRule": "^(!a-zA-Z)$"
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "establishedDate",
            "pretty_name": "establishedDate",
            "is_object_array": false,
            "json_key": "establishedDate",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ESTDT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "date",
            "column_type": "static"
        },
        {
            "display_label": "nonProfit",
            "pretty_name": "nonProfit",
            "is_object_array": false,
            "json_key": "nonProfit",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ISNONPRFT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "moneyServicesBusiness",
            "pretty_name": "moneyServicesBusiness",
            "is_object_array": false,
            "json_key": "moneyServicesBusiness",
            "column_family": "c",
            "isReportable": true,
            "column_name": "MNYSERBUS",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "industry",
            "pretty_name": "industry",
            "is_object_array": false,
            "json_key": "industry",
            "column_family": "c",
            "isReportable": true,
            "column_name": "IND",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "dunsNumber",
            "pretty_name": "dunsNumber",
            "is_object_array": false,
            "json_key": "dunsNumber",
            "column_family": "c",
            "isReportable": true,
            "column_name": "DUNSNUM",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "businessDescription",
            "pretty_name": "businessDescription",
            "is_object_array": false,
            "json_key": "businessDescription",
            "column_family": "c",
            "isReportable": true,
            "column_name": "BUSDESC",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "e3Data",
            "pretty_name": "e3Data",
            "is_object_array": false,
            "json_key": "e3Data",
            "column_family": "c",
            "isReportable": true,
            "column_name": "E3DAT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "riskDNA",
            "pretty_name": "riskDNA",
            "is_object_array": false,
            "json_key": "riskDNA",
            "column_family": "c",
            "isReportable": true,
            "column_name": "RDNA",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "e3BaseData",
            "pretty_name": "e3BaseData",
            "is_object_array": false,
            "json_key": "e3BaseData",
            "column_family": "c",
            "isReportable": true,
            "column_name": "E3RESPBASE",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "e3MasterId",
            "pretty_name": "e3MasterId",
            "is_object_array": false,
            "json_key": "e3MasterId",
            "column_family": "c",
            "isReportable": true,
            "column_name": "E3RESPRDNA",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "entityRiskDNAScore",
            "pretty_name": "entityRiskDNAScore",
            "is_object_array": false,
            "json_key": "entityRiskDNAScore",
            "column_family": "c",
            "isReportable": true,
            "column_name": "RDNASCO",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "entityRiskDNALevel",
            "pretty_name": "entityRiskDNALevel",
            "is_object_array": false,
            "json_key": "entityRiskDNALevel",
            "column_family": "c",
            "isReportable": true,
            "column_name": "RDNALVL",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "countryOfCitizenship",
            "pretty_name": "countryOfCitizenship",
            "is_object_array": false,
            "json_key": "countryOfCitizenship",
            "column_family": "c",
            "isReportable": true,
            "column_name": "CTRYCTZN",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "customerEndDate",
            "pretty_name": "customerEndDate",
            "is_object_array": false,
            "json_key": "customerEndDate",
            "column_family": "c",
            "isReportable": true,
            "column_name": "CUSTENDDT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "date",
            "column_type": "static"
        },
        {
            "display_label": "customerStatus",
            "pretty_name": "customerStatus",
            "is_object_array": false,
            "json_key": "customerStatus",
            "column_family": "c",
            "isReportable": true,
            "column_name": "CUSTSTAT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "language",
            "pretty_name": "language",
            "is_object_array": false,
            "json_key": "language",
            "column_family": "c",
            "isReportable": true,
            "column_name": "LANG",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "countryOfResidence",
            "pretty_name": "countryOfResidence",
            "is_object_array": false,
            "json_key": "countryOfResidence",
            "column_family": "c",
            "isReportable": true,
            "column_name": "CTRYOFRES",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "gender",
            "pretty_name": "gender",
            "is_object_array": false,
            "json_key": "gender",
            "column_family": "c",
            "isReportable": true,
            "column_name": "GNDR",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "maritalStatus",
            "pretty_name": "maritalStatus",
            "is_object_array": false,
            "json_key": "maritalStatus",
            "column_family": "c",
            "isReportable": true,
            "column_name": "MARSTAT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "deceasedStatus",
            "pretty_name": "deceasedStatus",
            "is_object_array": false,
            "json_key": "deceasedStatus",
            "column_family": "c",
            "isReportable": true,
            "column_name": "DECDSTAT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "deceasedDate",
            "pretty_name": "deceasedDate",
            "is_object_array": false,
            "json_key": "deceasedDate",
            "column_family": "c",
            "isReportable": true,
            "column_name": "DECDDT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "date",
            "column_type": "static"
        },
        {
            "display_label": "employmentStatus",
            "pretty_name": "employmentStatus",
            "is_object_array": false,
            "json_key": "employmentStatus",
            "column_family": "c",
            "isReportable": true,
            "column_name": "EMPSTAT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "numberDependents",
            "pretty_name": "numberDependents",
            "is_object_array": false,
            "json_key": "numberDependents",
            "column_family": "c",
            "isReportable": true,
            "column_name": "NOOFDEP",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "integer",
            "column_type": "static"
        },
        {
            "display_label": "legalStructure",
            "pretty_name": "legalStructure",
            "is_object_array": false,
            "json_key": "legalStructure",
            "column_family": "c",
            "isReportable": true,
            "column_name": "LEGLSTRUCT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "publiclyTraded",
            "pretty_name": "publiclyTraded",
            "is_object_array": false,
            "json_key": "publiclyTraded",
            "column_family": "c",
            "isReportable": true,
            "column_name": "PUBTR",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "placeOfLegalFormation",
            "pretty_name": "placeOfLegalFormation",
            "is_object_array": false,
            "json_key": "placeOfLegalFormation",
            "column_family": "c",
            "isReportable": true,
            "column_name": "PLOFLEGLFORM",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "sourceSystem",
            "pretty_name": "sourceSystem",
            "is_object_array": false,
            "json_key": "sourceSystem",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SRCSYS",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "sourceSystemId",
            "pretty_name": "sourceSystemId",
            "is_object_array": false,
            "json_key": "sourceSystemId",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SRCSYSID",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "sourceRecordId",
            "pretty_name": "sourceRecordId",
            "is_object_array": false,
            "json_key": "sourceRecordId",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SRCRECID",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "sourceRecordCreatedDate",
            "pretty_name": "sourceRecordCreatedDate",
            "is_object_array": false,
            "json_key": "sourceRecordCreatedDate",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SRCRECCDT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "date",
            "column_type": "static"
        },
        {
            "display_label": "sourceRecordModifiedDate",
            "pretty_name": "sourceRecordModifiedDate",
            "is_object_array": false,
            "json_key": "sourceRecordModifiedDate",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SRCRECMDT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "date",
            "column_type": "static"
        },
        {
            "display_label": "sourceRecordSystemStart",
            "pretty_name": "sourceRecordSystemStart",
            "is_object_array": false,
            "json_key": "sourceRecordSystemStart",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SRCRECSYSSTART",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "sourceRecordSystemEnd",
            "pretty_name": "sourceRecordSystemEnd",
            "is_object_array": false,
            "json_key": "sourceRecordSystemEnd",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SRCRECSYSEND",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "isClosed",
            "pretty_name": "isClosed",
            "is_object_array": false,
            "json_key": "isClosed",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ISCL",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "isDemarked",
            "pretty_name": "isDemarked",
            "is_object_array": false,
            "json_key": "isDemarked",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ISDMKTD",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "dob",
            "pretty_name": "dob",
            "is_object_array": false,
            "json_key": "dob",
            "column_family": "c",
            "isReportable": true,
            "column_name": "DOB",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "date",
            "column_type": "static"
        },
        {
            "display_label": "identificationDocs",
            "pretty_name": "identificationDocs",
            "is_object_array": false,
            "json_key": "identificationDocs",
            "column_family": "c",
            "isReportable": true,
            "column_name": "IDENT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_array",
            "column_type": "static"
        },
        {
            "is_object_array": true,
            "json_key": "associatedAccounts",
            "column_family": "c",
            "column_name": "accounts.{ID}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "associatedAccounts",
            "pretty_name": "associatedAccounts",
            "reverse_column_pattern": "accounts",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic",
            "reference_column_schema": "schema_accounts"
        },
        {
            "is_object_array": true,
            "json_key": "associatedEntities",
            "column_family": "c",
            "column_name": "assocentities.{ID}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "associatedEntities",
            "pretty_name": "associatedEntities",
            "reverse_column_pattern": "assocentities",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic",
            "reference_column_schema": "schema_entities"
        },
        {
            "is_object_array": true,
            "json_key": "entityReferences",
            "column_family": "c",
            "column_name": "entities.{ID}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "entityReferences",
            "pretty_name": "entityReferences",
            "reverse_column_pattern": "entities",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic",
            "reference_column_schema": "schema_entities"
        },
        {
            "is_object_array": true,
            "json_key": "phone",
            "column_family": "c",
            "column_name": "PHONE.{id}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "phone",
            "pretty_name": "phone",
            "reverse_column_pattern": "PHONE",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic"
        },
        {
            "is_object_array": true,
            "json_key": "email",
            "column_family": "c",
            "column_name": "EMAIL.{id}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "email",
            "pretty_name": "email",
            "reverse_column_pattern": "EMAIL",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic"
        },
        {
            "is_object_array": true,
            "json_key": "webLink",
            "column_family": "c",
            "column_name": "WEBLINK.{id}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "webLink",
            "pretty_name": "webLink",
            "reverse_column_pattern": "WEBLINK",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic"
        },
        {
            "is_object_array": true,
            "json_key": "address",
            "column_family": "c",
            "column_name": "ADDRESS.{id}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "address",
            "pretty_name": "address",
            "reverse_column_pattern": "ADDRESS",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic"
        },
        {
            "display_label": "industryTypes",
            "pretty_name": "industryTypes",
            "is_object_array": false,
            "json_key": "industryTypes",
            "column_family": "c",
            "isReportable": true,
            "column_name": "INDUS",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_array",
            "column_type": "static"
        },
        {
            "is_object_array": true,
            "json_key": "documents",
            "column_family": "c",
            "column_name": "documents.{ID}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "documents",
            "pretty_name": "documents",
            "reverse_column_pattern": "documents",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic",
            "reference_column_schema": "schema_documents"
        },
        {
            "display_label": "firstName",
            "pretty_name": "firstName",
            "is_object_array": false,
            "json_key": "firstName",
            "column_family": "c",
            "isReportable": true,
            "column_name": "FSTNM",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "lastName",
            "pretty_name": "lastName",
            "is_object_array": false,
            "json_key": "lastName",
            "column_family": "c",
            "isReportable": true,
            "column_name": "LSTNM",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "businessLegalName",
            "pretty_name": "businessLegalName",
            "is_object_array": false,
            "json_key": "businessLegalName",
            "column_family": "c",
            "isReportable": true,
            "column_name": "NATBUSLGLNM",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "suffix",
            "pretty_name": "suffix",
            "is_object_array": false,
            "json_key": "suffix",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SUFX",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "salutation",
            "pretty_name": "salutation",
            "is_object_array": false,
            "json_key": "salutation",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SALUT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "middleInitial",
            "pretty_name": "middleInitial",
            "is_object_array": false,
            "json_key": "middleInitial",
            "column_family": "c",
            "isReportable": true,
            "column_name": "MDLNM",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "isPrimary",
            "pretty_name": "isPrimary",
            "is_object_array": false,
            "json_key": "isPrimary",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ISPRI",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "id",
            "pretty_name": "id",
            "is_object_array": false,
            "json_key": "id",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ID",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "uuid",
            "pretty_name": "uuid",
            "is_object_array": false,
            "json_key": "uuid",
            "column_family": "c",
            "isReportable": true,
            "column_name": "UUID",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": true,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "createdBy",
            "pretty_name": "createdBy",
            "is_object_array": false,
            "json_key": "createdBy",
            "column_family": "c",
            "isReportable": true,
            "column_name": "CBY",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "lastModifiedBy",
            "pretty_name": "lastModifiedBy",
            "is_object_array": false,
            "json_key": "lastModifiedBy",
            "column_family": "c",
            "isReportable": true,
            "column_name": "MBY",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "bmoData",
            "pretty_name": "bmoData",
            "is_object_array": false,
            "json_key": "bmoData",
            "column_family": "x",
            "isReportable": true,
            "column_name": "bmoData",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "tdaData",
            "pretty_name": "tdaData",
            "is_object_array": false,
            "json_key": "tdaData",
            "column_family": "x",
            "isReportable": true,
            "column_name": "tdaData",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "changeType",
            "pretty_name": "changeType",
            "is_object_array": false,
            "json_key": "changeType",
            "column_family": "c",
            "isReportable": true,
            "column_name": "chgTyp",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "action",
            "pretty_name": "action",
            "is_object_array": false,
            "json_key": "action",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ACT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "snapshotInfo",
            "pretty_name": "snapshotInfo",
            "is_object_array": false,
            "json_key": "snapshotInfo",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SNAPINFO",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "type",
            "pretty_name": "type",
            "is_object_array": false,
            "json_key": "type",
            "column_family": "c",
            "isReportable": true,
            "column_name": "TYPE",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "changeLogItems",
            "pretty_name": "changeLogItems",
            "is_object_array": false,
            "json_key": "changeLogItems",
            "column_family": "c",
            "isReportable": true,
            "column_name": "changeLogItems",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_array",
            "column_type": "static"
        },
        {
            "display_label": "primaryInformation",
            "pretty_name": "primaryInformation",
            "is_object_array": false,
            "json_key": "primaryInformation",
            "column_family": "c",
            "isReportable": true,
            "column_name": "PRIINFO",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "secondaryInformation",
            "pretty_name": "secondaryInformation",
            "is_object_array": false,
            "json_key": "secondaryInformation",
            "column_family": "c",
            "isReportable": true,
            "column_name": "SECINFO",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "personas",
            "pretty_name": "personas",
            "is_object_array": false,
            "json_key": "personas",
            "column_family": "c",
            "isReportable": true,
            "column_name": "PERSONAS",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_array",
            "column_type": "static"
        },
        {
            "display_label": "dueDilligenceReports",
            "pretty_name": "dueDilligenceReports",
            "is_object_array": false,
            "json_key": "dueDilligenceReports",
            "column_family": "c",
            "isReportable": true,
            "column_name": "DUEDLGNC",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_array",
            "column_type": "static"
        },
        {
            "display_label": "businessMatch",
            "pretty_name": "businessMatch",
            "is_object_array": false,
            "json_key": "businessMatch",
            "column_family": "c",
            "isReportable": true,
            "column_name": "BUSMATCH",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_array",
            "column_type": "static"
        },
        {
            "display_label": "businessProfile",
            "pretty_name": "businessProfile",
            "is_object_array": false,
            "json_key": "businessProfile",
            "column_family": "c",
            "isReportable": true,
            "column_name": "BUSPROF",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_array",
            "column_type": "static"
        },
        {
            "display_label": "lastEnrichmentDate",
            "pretty_name": "lastEnrichmentDate",
            "is_object_array": false,
            "json_key": "lastEnrichmentDate",
            "column_family": "c",
            "isReportable": true,
            "column_name": "LENRCHDT",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "isDirty",
            "pretty_name": "isDirty",
            "is_object_array": false,
            "json_key": "isDirty",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ISDIRTY",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "riskDnaConfig",
            "pretty_name": "riskDnaConfig",
            "is_object_array": false,
            "json_key": "riskDnaConfig",
            "column_family": "c",
            "isReportable": true,
            "column_name": "RISKCONFIG",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "riskDNAThreshold",
            "pretty_name": "riskDNAThreshold",
            "is_object_array": false,
            "json_key": "riskDNAThreshold",
            "column_family": "c",
            "isReportable": true,
            "column_name": "RISKTHRESHOLD",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_object",
            "column_type": "static"
        },
        {
            "display_label": "affectedAccountNumber",
            "pretty_name": "affectedAccountNumber",
            "is_object_array": false,
            "json_key": "affectedAccountNumber",
            "column_family": "c",
            "isReportable": true,
            "column_name": "afftAccNum",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "display_label": "fullName",
            "pretty_name": "fullName",
            "is_object_array": false,
            "json_key": "fullName",
            "column_family": "c",
            "isReportable": true,
            "column_name": "FULLNM",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "string",
            "column_type": "static"
        },
        {
            "is_object_array": true,
            "json_key": "notes",
            "column_family": "c",
            "column_name": "NOTES.{id}",
            "description": "",
            "is_primary_unique": false,
            "type": "json_array",
            "display_label": "notes",
            "pretty_name": "notes",
            "reverse_column_pattern": "NOTES",
            "isReportable": true,
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "column_type": "dynamic"
        },
        {
            "display_label": "isPEP",
            "pretty_name": "isPEP",
            "is_object_array": false,
            "json_key": "isPEP",
            "column_family": "c",
            "isReportable": true,
            "column_name": "ISPEP",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "callIn",
            "pretty_name": "callIn",
            "is_object_array": false,
            "json_key": "callIn",
            "column_family": "c",
            "isReportable": true,
            "column_name": "CALLIN",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "boolean",
            "column_type": "static"
        },
        {
            "display_label": "tasks",
            "pretty_name": "tasks",
            "is_object_array": true,
            "json_key": "tasks",
            "column_family": "c",
            "column_name": "TASKSPROCIDS",
            "description": "",
            "extended_validation": {
                "validation_rule": "",
                "validated": false
            },
            "is_primary_unique": false,
            "type": "json_array",
            "column_type": "static",
            "shouldPopulatePossibleValues": true
        }
    ];
    selectedProperty = {
        display_name: '',
        column_name: '',
        json_key: '',
        description: '',
    };

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    showPropertyDetails(property: any) {
        this.selectedProperty = property;
    }
}
