import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolBarComponent } from './tool-bar/tool-bar.component';
import { RuleEditorComponent } from './rule-editor/rule-editor.component';
import { TestDataComponent } from './test-data/test-data.component';
import { RuleGroupComponent } from './rule-editor/rule-group/rule-group.component';
import { RuleComponent } from './rule-editor/rule-group/rule/rule.component';
import { IdentifierComponent } from './rule-editor/expression/identifier/identifier.component';
import { LiteralComponent } from './rule-editor/expression/literal/literal.component';
import { ExpressionComponent } from './rule-editor/expression/expression.component';
import { OperatorComponent } from './rule-editor/operator/operator.component';
import { EditRuleComponent } from './rule-editor/edit-rule/edit-rule.component';
import { DataManagementComponent } from './data-management/data-management.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { DataObjectsComponent } from './data-objects/data-objects.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ToolBarComponent,
    RuleEditorComponent,
    TestDataComponent,
    RuleGroupComponent,
    RuleComponent,
    IdentifierComponent,
    LiteralComponent,
    ExpressionComponent,
    OperatorComponent,
    EditRuleComponent,
    DataManagementComponent,
    NavbarComponent,
    DataObjectsComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
