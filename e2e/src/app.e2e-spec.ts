import { browser, by, element } from "protractor";

describe('AppComponent', () => {
  /** @test AppComponent should display the page title
   * When we visit /
   * Then the page title should be "Jarvis"
   */
  it('should display the page title', () => {
    browser.get('/');
    expect(browser.getTitle()).toEqual('Jarvis');
  });
});
